<?php
	define('HOST','localhost');
	define('USER','my_username');
	define('PASS','my_password');
	define('DB','my_db');

	$con = new mysqli(HOST, USER, PASS, DB);

	if ($con->connect_error)
	{
		die("Can not connect to the database".$con->connect_error);
	}

?>
