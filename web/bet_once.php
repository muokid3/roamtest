<?php

	if($_SERVER['REQUEST_METHOD']=='GET')
	{		
		$item= array();
		
		$jsonData = array();
	
		
		
		require_once('dbConnect.php');
		
		
		$sql = "SELECT *
			  FROM bets
			   WHERE date >= '2016-06-01 00:00:00' 
			      AND date < '2016-07-31 00:00:00'
			      GROUP BY profile
			      HAVING COUNT(DISTINCT profile) = 1
			      LIMIT 50";
		
		$res = $con->query($sql);
		$count = $res->num_rows;
		
		if ($count>0)
		{
			while ($row = $res->fetch_assoc())
			{
				$profile = $row['profile'];
				$amount= $row['amount'];			
				$odds= $row['odds'];
				$winnings= $row['winnings'];
				$source= $row['source'];
				$date = $row['date'];
				
				
				
				$item[profile] = $profile;
				$item[amount] = $amount;
				$item[odds] = $odds;
				$item[winnings] = $winnings;
				$item[source] = $source;
				$item[date] = $date;				
				
				$jsonData[] = $item;
			}
			
			echo json_encode($jsonData);
		}
		else
		{
			echo "empty";
			
		}
		
	

	}
	else
	{
		echo "unauthorised";
	}

?>