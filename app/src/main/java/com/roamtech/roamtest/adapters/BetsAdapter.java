package com.roamtech.roamtest.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.roamtech.roamtest.R;
import com.roamtech.roamtest.models.Bet;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by muoki on 10/9/16.
 */
public class BetsAdapter extends ArrayAdapter<Bet> {

    private List<Bet> bets;
    private Context context;
    public BetsAdapter(Context context, List<Bet> statementsList){
        super(context, R.layout.bets_item,statementsList);
        this.context = context;
        this.bets = statementsList;

    }
    // Hold views of the ListView to improve its scrolling performance
    static class ViewHolder {
        public TextView tv_title, tv_details, tv_amount;
        public Button button;

    }
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;
        // Inflate the list_item.xml file if convertView is null
        if(rowView==null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView= inflater.inflate(R.layout.bets_item, parent, false);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.tv_title = (TextView) rowView.findViewById(R.id.tv_title);
            viewHolder.tv_details = (TextView) rowView.findViewById(R.id.tv_description);
            rowView.setTag(viewHolder);

        }
        // Set text to each TextView of ListView item
        ViewHolder holder = (ViewHolder) rowView.getTag();
        holder.tv_title.setText("Profile No: "+bets.get(position).getProfile());
        holder.tv_details.setText("Source: "+bets.get(position).getSource());

        return rowView;
    }
}
