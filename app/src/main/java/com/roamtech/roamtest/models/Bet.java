package com.roamtech.roamtest.models;

import com.orm.SugarRecord;

/**
 * Created by muoki on 10/9/16.
 */

public class Bet extends SugarRecord {
    String profile, amount, odds, winnings, source, date;

    public Bet() {
    }

    public Bet(String profile, String amount, String odds, String winnings, String source, String date) {
        this.profile = profile;
        this.amount = amount;
        this.odds = odds;
        this.winnings = winnings;
        this.source = source;
        this.date = date;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getOdds() {
        return odds;
    }

    public void setOdds(String odds) {
        this.odds = odds;
    }

    public String getWinnings() {
        return winnings;
    }

    public void setWinnings(String winnings) {
        this.winnings = winnings;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
