package com.roamtech.roamtest;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.roamtech.roamtest.adapters.BetsAdapter;
import com.roamtech.roamtest.dependencies.Displays;
import com.roamtech.roamtest.dependencies.VolleyErrors;
import com.roamtech.roamtest.models.Bet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class SumPerSource extends AppCompatActivity {
    String sourceType;
    TextView title;
    SweetAlertDialog pDialog;
    public static String SUM_BASE_URL = "http://ryztek.com/roam/sum.php";
    private static final String KEY_SOURCE = "source";
    TextView januaryTxt,febTxt,marchTxt,aprilTxt,
            mayTxt,juneTxt,julyTxt,augTxt,septTxt,octTxt,novTxt,decTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sum_per_source);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        sourceType = intent.getStringExtra("source");

        title = (TextView) findViewById(R.id.title);
        title.setText("Sum of the amounts per month for "+sourceType);

        januaryTxt = (TextView) findViewById(R.id.january);
        febTxt = (TextView) findViewById(R.id.feb);
        marchTxt = (TextView) findViewById(R.id.march);
        aprilTxt = (TextView) findViewById(R.id.april);
        mayTxt = (TextView) findViewById(R.id.may);
        juneTxt = (TextView) findViewById(R.id.june);
        julyTxt = (TextView) findViewById(R.id.july);
        augTxt = (TextView) findViewById(R.id.aug);
        septTxt = (TextView) findViewById(R.id.sep);
        octTxt = (TextView) findViewById(R.id.oct);
        novTxt = (TextView) findViewById(R.id.nov);
        decTxt = (TextView) findViewById(R.id.dec);

        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Computing...");
        pDialog.setCancelable(false);

        pDialog.show();

        fetchSum();

    }

    private void fetchSum() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, SUM_BASE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response)
                    {
                        pDialog.dismiss();
                        try {

                            JSONArray itemDetailsArray = new JSONArray(response);


                            for (int i = 0; i < itemDetailsArray.length(); i++) {


                                JSONObject statementItem = (JSONObject) itemDetailsArray.get(i);

                                int jan = statementItem.getInt("jan");
                                int feb = statementItem.getInt("feb");
                                int mar = statementItem.getInt("mar");
                                int apr = statementItem.getInt("apr");
                                int may = statementItem.getInt("may");
                                int jun = statementItem.getInt("jun");
                                int jul = statementItem.getInt("jul");
                                int aug = statementItem.getInt("aug");
                                int sep = statementItem.getInt("sep");
                                int oct = statementItem.getInt("oct");
                                int nov = statementItem.getInt("nov");
                                int dec = statementItem.getInt("dec");

                                januaryTxt.setText("January: "+jan+"");
                                febTxt.setText("February: "+feb+"");
                                marchTxt.setText("March: "+mar+"");
                                aprilTxt.setText("April: "+apr+"");
                                mayTxt.setText("May: "+may+"");
                                juneTxt.setText("June: "+jun+"");
                                julyTxt.setText("July: "+jul+"");
                                augTxt.setText("August: "+aug+"");
                                septTxt.setText("September: "+sep+"");
                                octTxt.setText("October: "+oct+"");
                                novTxt.setText("November: "+nov+"");
                                decTxt.setText("December: "+dec+"");


                            }
                        } catch (JSONException e) {
                            pDialog.dismiss();
                            new SweetAlertDialog(SumPerSource.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Error")
                                    .setContentText(e.getMessage())
                                    .setConfirmText("Ok")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.dismiss();
                                            startActivity(new Intent(SumPerSource.this, MainActivity.class));
                                            finish();
                                        }

                                    })
                                    .show();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                pDialog.dismiss();

                Displays.displayErrorAlert("Error", VolleyErrors.getVolleyErrorMessages(error, SumPerSource.this), SumPerSource.this);

            }
        }){
            @Override
            protected Map<String, String> getParams()
            {
                Map <String, String> params = new HashMap<String, String>();
                params.put(KEY_SOURCE, sourceType);

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
