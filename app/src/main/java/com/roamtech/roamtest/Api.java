package com.roamtech.roamtest;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.roamtech.roamtest.adapters.BetsAdapter;
import com.roamtech.roamtest.dependencies.VolleyErrors;
import com.roamtech.roamtest.models.Bet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class Api extends AppCompatActivity {

    SweetAlertDialog pDialog, eDialog;
    public static String API_BASE_URL = "http://ryztek.com/roam/api.php";
    List<Bet> APIList;
    ListView listView;
    Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_api);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Fetching...");
        pDialog.setCancelable(false);

        APIList = new ArrayList<>();

        eDialog = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE);
        eDialog.setTitleText("Error");
        eDialog.setConfirmText("Try Again");
        eDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                startActivity(new Intent(Api.this, MainActivity.class));
                finish();
            }
        });

        listView = (ListView) findViewById(R.id.api_list);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bet clickedBet = (Bet) listView.getItemAtPosition(position);

                dialog = new Dialog(Api.this);
                dialog.setContentView(R.layout.bet_details);

                TextView profile = (TextView) dialog.findViewById(R.id.profile);
                profile.setText("Profile: "+clickedBet.getProfile());

                TextView amount = (TextView) dialog.findViewById(R.id.amount);
                amount.setText("Amount: "+clickedBet.getAmount());

                TextView odds = (TextView) dialog.findViewById(R.id.odds);
                odds.setText("Odds: "+clickedBet.getOdds());

                TextView winnings = (TextView) dialog.findViewById(R.id.winnings);
                winnings.setText("Winnings: "+clickedBet.getWinnings());

                TextView source = (TextView) dialog.findViewById(R.id.source);
                source.setText("Source: "+clickedBet.getSource());

                Button closeButton;
                closeButton = (Button) dialog.findViewById(R.id.close);
                closeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

        pDialog.show();

        fetchAPIList();
    }

    private void fetchAPIList() {

        StringRequest req = new StringRequest(Request.Method.GET, API_BASE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        try {

                            JSONArray itemDetailsArray = new JSONArray(response);


                            for (int i = 0; i < itemDetailsArray.length(); i++) {


                                JSONObject statementItem = (JSONObject) itemDetailsArray.get(i);

                                String profile = statementItem.getString("profile");
                                String amount = statementItem.getString("amount");
                                String odds = statementItem.getString("odds");
                                String winnings = statementItem.getString("winnings");
                                String source = statementItem.getString("source");
                                String date = statementItem.getString("date");


                                Bet newBet = new Bet(profile,amount,odds,winnings,source,date);

                                APIList.add(newBet);

                            }

                            ArrayAdapter<Bet> adapter = new BetsAdapter(Api.this, APIList);
                            listView.setAdapter(adapter);

                            pDialog.dismiss();
                        } catch (JSONException e) {
                            pDialog.dismiss();
                            new SweetAlertDialog(Api.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Error")
                                    .setContentText(e.getMessage())
                                    .setConfirmText("Ok")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.dismiss();
                                            startActivity(new Intent(Api.this, MainActivity.class));
                                            finish();
                                        }

                                    })
                                    .show();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                new SweetAlertDialog(Api.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error")
                        .setContentText(VolleyErrors.getVolleyErrorMessages(error, Api.this))
                        //.setContentText(error.getMessage())
                        .setConfirmText("Ok")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                                startActivity(new Intent(Api.this, MainActivity.class));
                                finish();
                            }

                        })
                        .show();
            }

        });
        req.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 5, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        RequestQueue requestQueue = Volley.newRequestQueue(Api.this);
        requestQueue.add(req);

    }
}
