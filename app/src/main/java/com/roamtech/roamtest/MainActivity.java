package com.roamtech.roamtest;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView syncTxt;
    private Button btnSMS,btnAPI,btnLastSixty,btnSumPerSource;
    Dialog dialog;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        syncTxt = (TextView)findViewById(R.id.syncTxt);

        btnSMS = (Button)findViewById(R.id.btnSMS);
        btnAPI = (Button)findViewById(R.id.btnAPI);
        btnLastSixty = (Button)findViewById(R.id.btnLastSixty);
        btnSumPerSource = (Button)findViewById(R.id.btnSumPerSource);




        btnSMS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, SmsList.class));

            }
        });

        btnSumPerSource.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new Dialog(MainActivity.this);
                dialog.setContentView(R.layout.select_source);

                Button apiButton;
                apiButton = (Button) dialog.findViewById(R.id.apiButton);
                apiButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent newIntent = new Intent(MainActivity.this, SumPerSource.class);
                        newIntent.putExtra("source","API_WEB");
                        startActivity(newIntent);
                    }
                });

                Button smsButton;
                smsButton = (Button) dialog.findViewById(R.id.smsButton);
                smsButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent newIntent = new Intent(MainActivity.this, SumPerSource.class);
                        newIntent.putExtra("source","SMS");
                        startActivity(newIntent);
                    }
                });


                Button closeButton;
                closeButton = (Button) dialog.findViewById(R.id.close);
                closeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();

            }
        });

        btnAPI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, Api.class));

            }
        });

        btnLastSixty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, LastSixty.class));

            }
        });
    }
}
